# Script Name: tea_time_class_2.R
# Purpose: Learning R
# Authors: Jack Mousseau



# install a collection of datasets
if (!require("datasets")) install.packages("datasets");
library(datasets)

# View the "mtcars" data frame
View(mtcars)


########################################################
## Create a line graph                                ##
########################################################

# Expose the columns of "mtcars" data set. Without this
# line of code one would have to access columns by
# mtcars$<name> where <name> is the column name. For
# instance: mtcars$mpg
attach(mtcars)

# Plot weight vs. mpg
plot(wt, mpg)

# Create a linear model
model = lm(mpg ~ wt)

# Add a straight line to the plot
abline(model)

# Set the plot title
title("Regression of MPG on weight")


########################################################
## Create a histogram                                 ##
########################################################

# Simple histogram
hist(mtcars$mpg)

# Colored Histogram with Different Number of Bins
hist(mtcars$mpg, breaks = 12, col = "blue")


########################################################
## Create a dot plot                                  ##
########################################################

# Simple Dotplot
dotchart(mtcars$mpg, labels = row.names(mtcars),cex = .7,
         main = "Gas Milage for Car Models",
         xlab = "Miles Per Gallon")

# Dotplot: Grouped Sorted and Colored
# Sort by mpg, group and color by cylinder

# sort by mpg
x = mtcars[order(mtcars$mpg),]

# cylinder must be a factor
x$cyl = factor(x$cyl)

# Set the color based on the number of cylinders
x$color[x$cyl == 4] = "red"
x$color[x$cyl == 6] = "blue"
x$color[x$cyl == 8] = "darkgreen"
dotchart(x$mpg,labels = row.names(x), cex = .7, groups = x$cyl,
         main = "Gas Milage for Car Models\ngrouped by cylinder",
         xlab = "Miles Per Gallon", gcolor = "black", color = x$color)


########################################################
## Create a dot plot                                  ##
########################################################

# Simple Pie Chart
slices = c(10, 12, 4, 16, 8)

# labels for the respective slices
lbls = c("US", "UK", "Australia", "Germany", "France")

# generate the pie chart
pie(slices, labels = lbls, main = "Pie Chart of Countries")


# Pie Chart with Percentages
# compute the percents
pct <- round(slices/sum(slices)*100)

# add percents to labels
lbls <- paste(lbls, pct)

# ad % to labels
lbls <- paste0(lbls, "%")

# generate the pie chart
pie(slices, labels = lbls, col = rainbow(length(lbls)),
    main="Pie Chart of Countries")



#########################################################
# GGplot2
#########################################################
install.packages("ggplot2")
library(ggplot2)


# CITE: http://blog.echen.me/2012/01/17/quick-introduction-to-ggplot2/

# Grab some example data to work with, as IRIS comes with ggplot2

head(iris) # by default, head displays the first 6 rows. see `?head`
head(iris, n = 10) # we can also explicitly set the number of rows to display

qplot(Sepal.Length, Petal.Length, data = iris)
# Plot Sepal.Length vs. Petal.Length, using data from the `iris` data frame.
# * First argument `Sepal.Length` goes on the x-axis.
# * Second argument `Petal.Length` goes on the y-axis.
# * `data = iris` means to look for this data in the `iris` data frame. 


qplot(Sepal.Length, Petal.Length, data = iris, color = Species)

plot1 = qplot(Sepal.Length, Petal.Length, data = iris, color = Species, size = Petal.Width, alpha = Sepal.Width)
# We see that Iris setosa flowers have the narrowest petals.


qplot(Sepal.Length, Petal.Length, data = iris, color = Species, size = Petal.Width, alpha = I(0.7))
# By setting the alpha of each point to 0.7, we reduce the effects of overplotting.

qplot(Sepal.Length, Petal.Length, data = iris, color = Species,
      xlab = "Sepal Length", ylab = "Petal Length",
      main = "Sepal vs. Petal Length in Fisher's Iris data")

## Another example with Orange Tree Data

# `Orange` is another built-in data frame that describes the growth of orange trees.
qplot(age, circumference, data = Orange, geom = "line",
      colour = Tree,
      main = "How does orange tree circumference vary with age?")


### More examples and class factor

# CITE: http://www.statmethods.net/advgraphs/ggplot2.html

# create factors with value labels
mtcars$gear <- factor(mtcars$gear,levels=c(3,4,5),
                      labels=c("3gears","4gears","5gears"))
mtcars$am <- factor(mtcars$am,levels=c(0,1),
                    labels=c("Automatic","Manual"))
mtcars$cyl <- factor(mtcars$cyl,levels=c(4,6,8),
                     labels=c("4cyl","6cyl","8cyl"))

# Kernel density plots for mpg
# grouped by number of gears (indicated by color)
qplot(mpg, data=mtcars, geom="density", fill=gear, alpha=I(.5),
      main="Distribution of Gas Milage", xlab="Miles Per Gallon",
      ylab="Density")

# Scatterplot of mpg vs. hp for each combination of gears and cylinders
# in each facet, transmittion type is represented by shape and color
qplot(hp, mpg, data=mtcars, shape=am, color=am,
      facets=gear~cyl, size=I(3),
      xlab="Horsepower", ylab="Miles per Gallon")

# Separate regressions of mpg on weight for each number of cylinders
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"),
      method="lm", formula=y~x, color=cyl,
      main="Regression of MPG on Weight",
      xlab="Weight", ylab="Miles per Gallon")

# Boxplots of mpg by number of gears
# observations (points) are overlayed and jittered
qplot(gear, mpg, data=mtcars, geom=c("boxplot", "jitter"),
      fill=gear, main="Mileage by Gear Number",
      xlab="", ylab="Miles per Gallon") 



### Real life example
# We need to get the data and make sure our working directory

# PRO TIP: Shortcut for changing working is "control+shift+h"

data.temp = read.csv("noaa_weather.csv")

data.temp$Date.Time = as.POSIXct(data.temp$Date.Time)


plot1 = ggplot(data = data.temp, aes(x = Date.Time, y =Temperature, colour = Climate)) +  
  facet_grid(Climate ~ ., scales = "free") + 
  geom_line(size = 0.75) + ggtitle("NOAA Temperature Data For Building Climates") +
  theme(axis.title=element_text(size = 15, face = "bold")) + ylab("Temperature (F)") + xlab("Date") +
  theme(plot.title=element_text(size = 15, face = "bold")) +
  theme(strip.text = element_text(size = 12, face = "bold")) +
  theme(legend.position = "none")
plot1

data1 = subset(data.temp, Climate == "Csb")

data.energy = data.temp
data.energy = data.energy[-c(1:96),]
data.energy = data.energy[-c((673):(length(data1[,1])-96)),]
data.energy = data.energy[-c((1345):length(data.energy[,1])),]


plot2 = ggplot(data = data.energy, aes(x = Date.Time, y =Temperature, colour = Climate)) +  
  facet_grid(Climate ~ ., scales = "free") + 
  geom_line(size = 0.75) + ggtitle("NOAA Temperature Data For Building Climates - One Week") +
  theme(axis.title=element_text(size = 15, face = "bold")) + ylab("Temperature (F)") + xlab("Date") +
  theme(plot.title=element_text(size = 15, face = "bold")) +
  theme(strip.text = element_text(size = 12, face = "bold")) +
  theme(legend.position = "none")
plot2

install.packages("Rmisc")
library(Rmisc)

multiplot(plot1, plot2)


### Creating a Dataframe on your own with intent to use ggplot

# Create matrix and convert to a dataframe
a = matrix(0, nrow = 30, ncol = 3)
a = as.data.frame(a)
# Rename all the columns to your variables
colnames(a)[1] = "XValue"
colnames(a)[2] = "YValue"
colnames(a)[3] = "Trig"

# Create an x value
x = seq(0, 6.28, by = 6.28/29)

# assign it to the xvalue column of a
a$XValue = x

# Lets create two copies of a in b and c
# Shortcut to formatting each one individually
b = c = a

# Create the three yvalue curves
y1 = sin(x)
y2 = cos(x)
y3 = y1/y2

# Set each one to its appropriate column
a$YValue = y1
b$YValue = y2
c$YValue = y3

# Label them with trig
a$Trig = "Sin(x)"
b$Trig = "Cos(x)"
c$Trig = "Tan(x)"

View(a)

# Now we use rbind to bind all the rows together into one data frame
trig = rbind(a,b,c)

View(trig)

# Lets plot it in ggplot
plot.trig = ggplot(data = trig, aes(x = XValue, y = YValue, colour = Trig)) + geom_line(size = 1.1)
# Make sure geom_line() is there or else nothing will happen. 

plot.trig

# Lets change the axes, the additive nature allows us just to put it "on top" of the previous plot
plot.trig + ylim(-1.5, 1.5)
plot.trig + ylim(-5,5)
plot.trig + ylim("none")

# Reset ther plot
plot.trig = ggplot(data = trig, aes(x = XValue, y = YValue, color = Trig)) + geom_line(size = 1.1)

plot.trig

# Lets make it all nice
plot.trig + ggtitle("Trig as a Function of X") +
  theme(axis.title=element_text(size = 18, face = "bold"))  +
  theme(axis.text=element_text(size = 18)) +
  theme(plot.title=element_text(size = 18, face = "bold")) +
  theme(legend.text=element_text(size= 18)) +
  theme(legend.title=element_text(size= 18, face = "bold"))

# Save it and publish in Nature
png("trig.png", width = 7, height = 5, units = "in", res = 400)
plot.trig
dev.off()